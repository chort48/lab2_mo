import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'labaSecond',
    component: () => import('../views/labaSecond.vue')
  },
  {
    path: '/Broeden',
    name: 'labaThird',
    component: () => import('../views/labaThird')
  },
  {
    path: '/BFGS',
    name: 'labaFourth',
    component: () => import('../views/labaFourth')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
