import calculator_formula from "./calculator_formula"
import {derivative, sqrt} from 'mathjs'
export default {
    method(f, start_point, e, variable){
        this.f = f
        this.start_point = start_point
        this.x = JSON.parse(JSON.stringify(start_point))
        this.e = e
        this.H = this.unit_matrix()
        this.variable = variable
        this.delta_F = []
        this.delta_F_val = []
        this.p = []
        this.history = {x:[], H:[], delta_F_val:[], F:[]}
        this.variable.forEach((value) => {
            this.delta_F.push(derivative(this.f, value).toString())
        })
        for(let item of this.delta_F){
            this.delta_F_val.push(calculator_formula.calculator(item, this.start_point, this.variable, 9999999))
        }
        let iter = 100
        this.saveHistory()
        while(iter > 0 && this.exit()){
            this.p = this.point_search()
            let interval = this.searchInterval()
            this.x = this.calculator_newPoint(this.methodDitohomi(interval))

            let delta_F_val_i = []
            for(let item of this.delta_F){
                delta_F_val_i.push(calculator_formula.calculator(item, this.x, this.variable,9999999))
            }

            let s = []
            let y = []
            let znam = 0
            for(let i = 0; i < this.delta_F_val.length; ++i){
                y.push(delta_F_val_i[i]-this.delta_F_val[i])
                s.push(this.x[i] - this.start_point[i])
                znam += y[i]*s[i]
            }
            let k = 1/znam
            this.H = this.matrix_Gisse(k, s, y)
            this.start_point = JSON.parse(JSON.stringify(this.x))

            let i = 0
            this.delta_F_val = delta_F_val_i

            this.saveHistory()
            iter--
        }
        return {history: this.history, point:this.x}
    },
    point_search(){
        let answer_p = []
        for(let item of this.H){
            let sum = 0
            for(let index = 0; index < this.delta_F.length; ++index){
                sum += -item[index] * this.delta_F_val[index]
            }
            answer_p.push(sum)
        }
        return answer_p
    },
    unit_matrix(){
        let matrix = []
        for(let i = 0; i < this.x.length; ++i){
            let arr = []
            for(let j = 0; j < this.x.length; ++j){
                if(i == j)
                    arr.push(1)
                else
                    arr.push(0)
            }
            matrix.push(arr)
        }
        return matrix
    },
    calculator_newPoint(lymda){
        let point = []
        for(let i = 0; i < this.x.length; ++i){
            point.push(this.x[i] + lymda*this.p[i])
        }
        return point
    },
    searchInterval(){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let step = 1
        let lymda = 1
        let h = 1
        let b
        let a

        let f_c
        let f_a
        let f_b

        startPoint = this.calculator_newPoint(lymda)
        f_c = calculator_formula.calculator(this.f , startPoint, this.variable, 9999999)

        b = JSON.parse(JSON.stringify(lymda)) + h
        startPoint = this.calculator_newPoint(b)
        f_b = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)


        if(f_b < f_c)
            step = 1
        else {
            step = 2

            a = JSON.parse(JSON.stringify(lymda)) - h
            startPoint = this.calculator_newPoint(a)
            f_a = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

            if(f_a >= f_c)
                step = 3
        }
        let k = 1000
        while(k !== 0 && step !== 3){
            // console.log(step, a, b, f_a, f_c, f_a)
            switch (step) {
                case 1:
                    a = JSON.parse(JSON.stringify(lymda))
                    f_a = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(b))
                    f_c = JSON.parse(JSON.stringify(f_b))
                    h *= 2

                    b = JSON.parse(JSON.stringify(lymda)) + h
                    startPoint = this.calculator_newPoint(b)
                    f_b = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

                    if(f_b >= f_c)
                        step = 3
                    break
                case 2:
                    b = JSON.parse(JSON.stringify(lymda))
                    f_b = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(a))
                    f_c = JSON.parse(JSON.stringify(f_a))
                    h *= 2

                    a = JSON.parse(JSON.stringify(lymda)) - h
                    startPoint = this.calculator_newPoint(a)
                    f_a = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

                    if(f_a >= f_c)
                        step = 3
                    break
            }
            k--
        }
        if(k === 0){
            if(Math.abs(a) >= Math.abs(b)) {
                b = a
            }
            else{
                a = b
            }
        }
        return [a, b]

    },
    methodDitohomi(interval){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let e = 0.00001
        let h = 0.0001
        let a = interval[0]
        let b = interval[1]
        let x1 = (a+b-h)/2
        let x2 = (a+b+h)/2
        let e_znam = 2
        while (Math.abs(b-a)/e_znam > e){
            startPoint = this.calculator_newPoint(x1)
            let f1 = calculator_formula.calculator(this.f , startPoint, this.variable, 9999999)

            startPoint = this.calculator_newPoint(x2)
            let f2 = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

            if(f1 < f2)
                b = x2
            else
                a = x1
            x1 = (a+b-h)/2
            x2 = (a+b+h)/2
            e_znam *= 2
        }
        return (a + b)/2
    },
    matrix_Gisse(k, s, y){
        let s_y = []
        let y_s = []
        let s_s = []
        let I = this.unit_matrix()
        for(let i = 0; i < s.length; ++i){
            let arr_sy = []
            let arr_ys = []
            let arr_ss = []
            for (let j = 0; j < s.length; ++j){
                arr_sy.push(I[i][j] - k*s[i]*y[j])
                arr_ys.push(I[i][j] - k*s[j]*y[i])
                arr_ss.push(k*s[i]*s[j])
            }
            s_y.push(arr_sy)
            y_s.push(arr_ys)
            s_s.push(arr_ss)
        }
        let H_s_y = []
        for (let i = 0; i < s.length; ++i){
            let arr = []
            for(let j = 0; j < s.length; ++j){
                let sum = 0
                for (let c = 0; c < s.length; ++c){
                    sum += s_y[i][c]*this.H[c][j]
                }
                arr.push(sum)
            }
            H_s_y.push(arr)
        }
        let H = []
        for (let i = 0; i < s.length; ++i){
            let arr = []
            for(let j = 0; j < s.length; ++j){
                let sum = 0
                for (let c = 0; c < s.length; ++c){
                    sum += H_s_y[i][c]*y_s[c][j]
                }
                arr.push(sum)
            }
            H.push(arr)
            for(let j = 0; j < s.length; ++j){
                H[i][j] += s_s[i][j]
            }
        }
        return H
    },
    exit(){
        let sum = 0
        for(let item of this.delta_F_val){
            sum += item*item
        }
        sum = sqrt(sum)
        if(sum <= this.e)
            return false
        return  true
    },
    saveHistory(){
        this.history.x.push(this.x)
        this.history.H.push(this.H)
        this.history.delta_F_val.push(this.delta_F_val)
        this.history.F.push(calculator_formula.calculator(this.f , this.x, this.variable, 9999999))
    }
}