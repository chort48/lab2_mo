import calculator_formula from "./calculator_formula"
import {derivative, sqrt, max} from 'mathjs'
import { Vector, Quatern, Matrix } from 'javascript-algebra'
import matrix from 'matrix-js'
export default {
    method(f, start_point, e, variable){
        this.f = f
        this.start_point = start_point
        this.x = JSON.parse(JSON.stringify(start_point))
        this.e = e
        this.variable = variable
        this.p = []
        this.Jacob = []
        this.A = []
        this.history = {x:[], A:[], F:[]}
        for(let item of this.f)
        {
            let arr = []
            this.variable.forEach((value) => {
                arr.push(derivative(item, value).toString())
            })
            this.Jacob.push(arr)
        }
        for(let item of this.Jacob)
        {
            let arr = []
            for (let item_i of item)
                arr.push(calculator_formula.calculator(item_i, this.start_point, this.variable, 0))
            this.A.push(arr)
        }
        let iter = 100
        this.saveHistory()
        while (iter !== 0 && this.exit()){
            let A_obr = this.Fadeev()
            let F = this.calculator_F()
            let s = this.chislStep(A_obr, F)
            this.x = this.newPoint(s)
            let F_1 = this.calculator_F()
            let y = []
            for (let i = 0; i < F.length; ++i){
                y.push([F_1[i] - F[i]])
            }
            this.A = this.newMatrixA(y, s)
            this.saveHistory()
            iter--
        }
        return {history: this.history, point:this.x}
    },
    calculator_F(){
        let F = []
        for(let item of this.f) {
            F.push(calculator_formula.calculator(item, this.x, this.variable, 0))
        }
        return F
    },
    chislStep(A_obr, F){
        let s = []
        for(let i = 0; i < A_obr.length; ++i){
            let sum = 0
            for (let j = 0; j < A_obr[0].length; ++j){
                sum += A_obr[i][j]*F[j]
            }
            s.push(-sum)
        }
        return s
    },
    newPoint(s){
        let point = []
        for(let i = 0; i < s.length; ++i){
            point.push(this.x[i] + s[i])
        }
        return point
    },
    newMatrixA(y, s){
        let c = []
        for(let item of s)
            c.push([item])
        let A =  matrix(this.A)
        let Y =  matrix(y)
        let S =  matrix(c)

        return A.add( matrix (matrix (matrix ( Y.sub( matrix( A.prod(S) ) ) ).prod(matrix(S.trans()))).map(x => x / matrix(S.trans()).prod(S)) ) )
    },
    Fadeev(){
        let A = matrix(this.A)
        let I = this.identity(max(this.A.length, this.A[0].length))
        let A_T = matrix(A.trans())
        let A_T_A = matrix(A_T.prod(A))
        let F = this.identity(max(this.A.length, this.A[0].length))
        let mu = this.trace(A_T_A())
        let _mu
        let _F
        let i = 2
        while (mu > 0.0000001){
            if( this.trace( A_T_A.prod( matrix(  matrix( I.map(x => x*mu) ).sub( matrix( A_T_A.prod(F) ) ) ) ) )/i  < 0.0000001) {
                _mu = mu
                _F = F
            }
            F = matrix(  matrix( I.map(x => x*mu) ).sub( matrix( A_T_A.prod(F) ) ) )
            mu = this.trace(A_T_A.prod(F))/i

            i++
        }
        let D = matrix(_F.prod(A_T)).map(x => x/_mu)
        return D
    },
    identity(n){
        let I = []
        for (let i = 0; i < n; ++i){
            let arr = []
            for (let j = 0; j < n; ++j){
                if(i == j)
                    arr.push(1)
                else
                    arr.push(0)
            }
            I.push(arr)
        }
        return matrix(I)
    },
    trace(A){
      let sum = 0
      for (let i = 0; i < A.length; ++i){
          sum += A[i][i]
      }
      return sum
    },
    exit(){
        let sum = 0
        let F = this.calculator_F()
        for(let item of F){
            sum += item*item
        }
        sum = sqrt(sum)
        if(sum <= this.e)
            return false
        return  true
    },
    saveHistory(){
        this.history.x.push(this.x)
        this.history.A.push(this.A)
        this.history.F.push(this.calculator_F())
    }
}