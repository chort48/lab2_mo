import calculator_formula from "./calculator_formula"
import {derivative, sqrt} from 'mathjs'

export default {
    method(f, start_point, e, variable){
        this.f = f
        this.start_point = start_point
        this.x = JSON.parse(JSON.stringify(start_point))
        this.e = e
        this.variable = variable
        this.p = []
        this.delta_F = []
        this.delta_F_val = []
        this.history = {x:[], delta_F_val:[], F:[]}

        this.variable.forEach((value) => {
            this.delta_F.push(derivative(this.f, value).toString())
        })
        for(let item of this.delta_F){
            this.delta_F_val.push(calculator_formula.calculator(item, this.start_point, this.variable, 0))
        }
        let iter = 100

        this.p = JSON.parse(JSON.stringify(this.delta_F_val))
        this.p = this.point_search(0)

        this.saveHistory()
        while(iter > 0 && this.exit()){
            let interval = this.searchInterval()
            this.x = this.calculator_newPoint(this.methodDitohomi(interval))

            let delta_F_val_new = []

            for(let item of this.delta_F){
                delta_F_val_new.push(calculator_formula.calculator(item, this.x, this.variable, 0))
            }

            let b = this.calculator_b(delta_F_val_new)

            this.delta_F_val = JSON.parse(JSON.stringify(delta_F_val_new))

            this.p = this.point_search(b)
            this.saveHistory()
        }
        return {history: this.history, point:this.x}
    },
    point_search(b){
        let answer_p = []
        for(let i = 0; i < this.delta_F_val.length; ++i){
            answer_p.push(-this.delta_F_val[i] + b*this.p[i])
        }
        return answer_p
    },
    calculator_b(delta_F_val_new){
        let sum1 = 0
        let sum2 = 0

        for (let i = 0; i < this.delta_F_val.length; ++i){
            sum1 += delta_F_val_new[i]*delta_F_val_new[i]
            sum2 += this.delta_F_val[i]*this.delta_F_val[i]
        }
        return sum1/sum2
    },
    calculator_newPoint(lymda){
        let point = []
        for(let i = 0; i < this.x.length; ++i){
            point.push(this.x[i] + lymda*this.p[i])
        }
        return point
    },
    exit(){
      let sum = 0
      for (let item of this.delta_F_val)
          sum += item*item
       if(sqrt(sum) < this.e)
           return false
        return true
    },
    searchInterval(){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let step = 1
        let lymda = 1
        let h = 1
        let b
        let a

        let f_c
        let f_a
        let f_b

        startPoint = this.calculator_newPoint(lymda)
        f_c = calculator_formula.calculator(this.f , startPoint, this.variable, 0)

        b = JSON.parse(JSON.stringify(lymda)) + h
        startPoint = this.calculator_newPoint(b)
        f_b = calculator_formula.calculator(this.f , startPoint, this.variable,0)


        if(f_b < f_c)
            step = 1
        else {
            step = 2

            a = JSON.parse(JSON.stringify(lymda)) - h
            startPoint = this.calculator_newPoint(a)
            f_a = calculator_formula.calculator(this.f , startPoint, this.variable,0)

            if(f_a >= f_c)
                step = 3
        }
        let k = 1000
        while(k !== 0 && step !== 3){
            // console.log(step, a, b, f_a, f_c, f_a)
            switch (step) {
                case 1:
                    a = JSON.parse(JSON.stringify(lymda))
                    f_a = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(b))
                    f_c = JSON.parse(JSON.stringify(f_b))
                    h *= 2

                    b = JSON.parse(JSON.stringify(lymda)) + h
                    startPoint = this.calculator_newPoint(b)
                    f_b = calculator_formula.calculator(this.f , startPoint, this.variable,0)

                    if(f_b >= f_c)
                        step = 3
                    break
                case 2:
                    b = JSON.parse(JSON.stringify(lymda))
                    f_b = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(a))
                    f_c = JSON.parse(JSON.stringify(f_a))
                    h *= 2

                    a = JSON.parse(JSON.stringify(lymda)) - h
                    startPoint = this.calculator_newPoint(a)
                    f_a = calculator_formula.calculator(this.f , startPoint, this.variable,0)

                    if(f_a >= f_c)
                        step = 3
                    break
            }
            k--
        }
        if(k === 0){
            if(Math.abs(a) >= Math.abs(b)) {
                b = a
            }
            else{
                a = b
            }
        }
        return [a, b]

    },
    methodDitohomi(interval){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let e = 0.00001
        let h = 0.0001
        let a = interval[0]
        let b = interval[1]
        let x1 = (a+b-h)/2
        let x2 = (a+b+h)/2
        let e_znam = 2
        while (Math.abs(b-a)/e_znam > e){
            startPoint = this.calculator_newPoint(x1)
            let f1 = calculator_formula.calculator(this.f , startPoint, this.variable, 0)

            startPoint = this.calculator_newPoint(x2)
            let f2 = calculator_formula.calculator(this.f , startPoint, this.variable,0)

            if(f1 < f2)
                b = x2
            else
                a = x1
            x1 = (a+b-h)/2
            x2 = (a+b+h)/2
            e_znam *= 2
        }
        return (a + b)/2
    },
    saveHistory(){
        this.history.x.push(this.x)
        this.history.delta_F_val.push(this.delta_F_val)
        this.history.F.push(calculator_formula.calculator(this.f , this.x, this.variable, 0))
    }
}