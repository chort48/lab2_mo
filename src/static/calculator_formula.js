export default {
    setVariable(x)
    {
        let i = 0
        this.variable.forEach((value) => {
            this.parser.setVariable(value, x[i])
            i++
        })
    },
    calculator(f, x, variable, default_ = 0)
    {
        let FormulaParser = require('hot-formula-parser').Parser;
        this.parser = new FormulaParser();
        this.variable = variable
        this.setVariable(x)
        f = this.parser.parse(f)
        return f.result ?? default_

    }
}