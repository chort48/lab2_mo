import calculator_formula from "./calculator_formula";
export default {
    method(f, x, e, variable){
        this.f = f
        this.x = x
        this.x_star = JSON.parse(JSON.stringify(x))
        this.e = e
        this.variable = variable
        this.s = []

        let X = []
        let F_X = []

        for(let i = 0; i < this.x.length; ++i){
            let arr = []
            for(let j = 0; j < this.x.length; ++j){
                if(i === j)
                    arr.push(1)
                else
                    arr.push(0)
            }
            this.s.push(arr)
        }

        let k = 1000
        do{
            let delta = 0
            let s_m = 0
            let min_x_f = JSON.parse(JSON.stringify(this.x))
            let s_k_min = 0
            let x_f = 0
            for(let i = 0; i < this.s.length; ++i){
                let x_old = JSON.parse(JSON.stringify(this.x))
                let interval = this.searchInterval(i)
                this.x = this.calculator(i, this.methodDitohomi(i, interval))

                let f_old = calculator_formula.calculator(this.f , x_old, this.variable,9999999)

                let f_new = calculator_formula.calculator(this.f , x_old, this.variable,9999999)

                if(i === 0 || delta < Math.abs(f_new - f_old)) {
                    delta = Math.abs(f_new - f_old)
                    s_m = this.s[i]
                    x_f = JSON.parse(JSON.stringify(this.x))
                }

                if(f_old > f_new) {
                    min_x_f = JSON.parse(JSON.stringify(this.x))
                    s_k_min = this.s[i]
                }
            }

            let x_n1 = []
            for(let i = 0; i < this.x.length; ++i){
                x_n1.push(2*this.x[i] - this.x_star)
            }

            let f1 = calculator_formula.calculator(this.f , this.x_star, this.variable, 9999999)

            let f2 = calculator_formula.calculator(this.f , this.x,  this.variable,9999999)

            let f3 = calculator_formula.calculator(this.f , x_n1,  this.variable,9999999)

            if(f3 >= f1 || (f1 - 2*f2 + f3)*Math.pow((f1-f2-delta), 2) > 0.5*delta*Math.pow(f1-f3, 2)){
                if(f2 < f3){
                    X.push(this.x)
                    F_X.push(calculator_formula.calculator(this.f , this.x,  this.variable,9999999))
                    if(!this.exitCheck())
                        break
                    this.x_star = JSON.parse(JSON.stringify(this.x))
                }
                else{
                    this.x = JSON.parse(JSON.stringify(x_n1))
                    X.push(this.x_star)
                    F_X.push(calculator_formula.calculator(this.f , this.x,  this.variable,9999999))
                    if(!this.exitCheck())
                        break
                    this.x_star = JSON.parse(JSON.stringify(x_n1))
                }
            }
            else{
                this.x = JSON.parse(JSON.stringify(min_x_f))
                if(!this.exitCheck())
                    break
                this.x_star = JSON.parse(JSON.stringify(min_x_f))
            }
            k--
        }
        while (k > 0)
        return {history:[X, F_X], point:this.x}
    },
    searchInterval(i){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let step = 1
        let lymda = 1
        let h = 1
        let b
        let a

        let f_c
        let f_a
        let f_b

        startPoint = this.calculator(i, lymda)
        f_c = calculator_formula.calculator(this.f , startPoint, this.variable, 9999999)

        b = JSON.parse(JSON.stringify(lymda)) + h
        startPoint = this.calculator(i, b)
        f_b = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)


        if(f_b < f_c)
            step = 1
        else {
            step = 2

            a = JSON.parse(JSON.stringify(lymda)) - h
            startPoint = this.calculator(i, a)
            f_a = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

            if(f_a >= f_c)
                step = 3
        }
        let k = 1000
        while(k !== 0 && step !== 3){
            // console.log(step, a, b, f_a, f_c, f_a)
            switch (step) {
                case 1:
                    a = JSON.parse(JSON.stringify(lymda))
                    f_a = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(b))
                    f_c = JSON.parse(JSON.stringify(f_b))
                    h *= 2

                    b = JSON.parse(JSON.stringify(lymda)) + h
                    startPoint = this.calculator(i, b)
                    f_b = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

                    if(f_b >= f_c)
                        step = 3
                    break
                case 2:
                    b = JSON.parse(JSON.stringify(lymda))
                    f_b = JSON.parse(JSON.stringify(f_c))
                    lymda = JSON.parse(JSON.stringify(a))
                    f_c = JSON.parse(JSON.stringify(f_a))
                    h *= 2

                    a = JSON.parse(JSON.stringify(lymda)) - h
                    startPoint = this.calculator(i, a)
                    f_a = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

                    if(f_a >= f_c)
                        step = 3
                    break
            }
            k--
        }
        if(k === 0){
            if(Math.abs(a) >= Math.abs(b)) {
                b = a
            }
            else{
                a = b
            }
        }
        return [a, b]

    },
    methodDitohomi(i, interval){
        let startPoint = JSON.parse(JSON.stringify(this.x))
        let e = 0.00001
        let h = 0.0001
        let a = interval[0]
        let b = interval[1]
        let x1 = (a+b-h)/2
        let x2 = (a+b+h)/2
        let e_znam = 2
        while (Math.abs(b-a)/e_znam > e){
            startPoint = this.calculator(i, x1)
            let f1 = calculator_formula.calculator(this.f , startPoint, this.variable, 9999999)

            startPoint = this.calculator(i, x2)
            let f2 = calculator_formula.calculator(this.f , startPoint, this.variable,9999999)

            if(f1 < f2)
                b = x2
            else
                a = x1
            x1 = (a+b-h)/2
            x2 = (a+b+h)/2
            e_znam *= 2
        }
        return (a + b)/2
    },
    calculator(i, lymda){
        let point = []
        for(let item = 0; item < this.x.length; ++item){
            point.push(this.x[item] + this.s[i][item]*lymda)
        }
        return point
    },
    exitCheck(){
        for(let i = 0; i < this.x.length; ++i){
            if(Math.abs(this.x[i] - this.x_star[i]) > this.e){
                console.log('2313')
                return true
            }
            else
                return false
        }
    }
}