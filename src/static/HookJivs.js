export default {
    method(f, x, delta, e, alpha, variable){
        let FormulaParser = require('hot-formula-parser').Parser;

        this.parser = new FormulaParser();
        this.f = f
        this.x = x
        this.x_i1 = JSON.parse(JSON.stringify(x))
        this.delta = delta
        this.e = e
        this.alpha = alpha
        this.variable =variable

        let X = []
        let F_X = []

        let k=1000
        while(this.check() && k !== 0){
            this.exploratorySearch()

            this.setVariable(this.x)
            let f1 = this.parser.parse(this.f).result


            this.setVariable(this.x_i1)
            let f2 = this.parser.parse(this.f).result

            if(f1 > f2){
                let b = JSON.parse(JSON.stringify(this.x_i1))
                for(let item in this.x){
                    this.x_i1[item] = this.x_i1[item] + (this.x_i1[item] - this.x[item])
                }
                this.x = b

                X.push(this.x)
                this.setVariable(this.x)
                F_X.push(this.parser.parse(this.f).result)
            }
            else {
                this.x_i1 = JSON.parse(JSON.stringify(this.x))
                for(let item in this.delta){
                    if(this.delta[item] >= this.e)
                        this.delta[item] /= this.alpha
                }
            }
            k--
        }
        return {history:[X, F_X], point:this.x}
    },
    exploratorySearch(){
        this.setVariable(this.x_i1)
        let y = this.parser.parse(this.f).result
        let y_i = 0
        for(let i = 0; i < this.x.length; ++i) {
            let x_i = JSON.parse(JSON.stringify(this.x_i1))
            x_i[i] = this.x_i1[i] + this.delta[i]
            this.setVariable(x_i)
            y_i = this.parser.parse(this.f).result
            if (y_i < y) {
                this.x_i1[i] = x_i[i]
                y = y_i
                continue
            }

            x_i[i] = this.x_i1[i] - this.delta[i]
            this.setVariable(x_i)
            y_i = this.parser.parse(this.f).result
            if (y_i < y) {
                this.x_i1[i] = x_i[i]
                y = y_i
            }
        }
    },
    check(){
        for(let item of this.delta){
            if(item >= this.e)
                return true
        }
        return false
    },
    setVariable(x){
        let i = 0
        this.variable.forEach((value)=>{
            this.parser.setVariable(value, x[i])
            i++
        })
    }
}